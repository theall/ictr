﻿#!/usr/bin/env Python
# coding=utf-8
import urllib.request as urllib2
from urllib import parse
import re
import json 
import time
import utils

def getBody(token, user, password):
    test_data = {
        '__RequestVerificationToken': token, 
        'UserName':user, 
        'PassWord':password, 
        'mobilePhoneNumber': '',
        'validateCode': '',
        'X-Requested-With':'XMLHttpRequest'}
    test_data_urlencode = parse.urlencode(test_data)
    return test_data_urlencode
    
def getToken(s):
    pattern = re.compile(r".+\<input +name=\"__RequestVerificationToken\" +type=\"hidden\" +value=\"(.+?)\" +/\>")
    ret = re.findall(pattern, s)
    if type(ret)==list and len(ret)>0:
        return ret[-1]
    return ''
    
def fillHeader():
    return {
        "Host": "www.ictr.cn",
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "http://www.ictr.cn/",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "zh,zh-CN;q=0.9",
        "Cookie": "ASP.NET_SessionId=2xw5tzallvdsx210ie5tu2ez; __RequestVerificationToken=eOtLoqnG_oFGdKTwhspBKpUbF-UQobZfEnoigWZ9Tm2pNqrfVMkxvK_AZde0A-XsTMSgyepnHC6awrKn9IUN0niBAyl2mKIhlChLoP7O8eU1; _ga=GA1.2.1207133948.1525946359; _gid=GA1.2.970083091.1525946359; browserId=a1dc8c84f94f48f9af02ab14e2329c67; Hm_lvt_eaf4daa932c555e535009b656dc5a9fb=1525942490; NAME_USER=NAME_USER=1235038; userId=|d41df65f-b0e2-43d7-bac5-2041f023f39a; Hm_lpvt_eaf4daa932c555e535009b656dc5a9fb=1525948648"
    }
    
def checkResult(res):
    try:
        res = json.loads(res)
        if type(res)!=dict and not res.get('Success', False):
            return False
        return True
    except:
        pass
    return False
    
def accessUrl(remoteUrl, body=''):
    res = ''
    try:
        req = urllib2.Request(url=remoteUrl, headers=fillHeader(), data=body.encode())
        ret_data = urllib2.urlopen(req)
        res = ret_data.read().decode('gb2312')
    except Exception as e:
        print(e)
    return res
     
def parseLotteryResult(result):
    money = utils.findMatchAsInteger(result, r"在幸运大转盘活动中获得(\d+)元网站现金")
    beans = utils.findMatchAsInteger(result, r"在幸运大转盘活动中获得(\d+)个金豆")
    return (money, beans)
    
def main():
    accounts_list = utils.findAccountsList()
    if len(accounts_list)<1:
        print("No accounts found!")
        return
    else:
        print("Find %d accounts"%len(accounts_list))
        
    money = 0
    beans = 0
    for account in accounts_list:
        res = accessUrl("http://www.ictr.cn/")
        token = getToken(res)
        if token=='':
            print("Can not find token from index page")
            continue
        else:
            pass
            #print("Find token: %s"%token)
        
        time.sleep(2)
        res = accessUrl("http://www.ictr.cn/Home/Login", getBody(token, account[0], account[1]))
        if not checkResult(res):
            print("Login failed!Number: %s"%account[0])
            continue
        else:
            print("Login success!Number: %s"%account[0])
        
        time.sleep(3)
        # Sign
        accessUrl("http://www.ictr.cn/MemberCenter/IsRegistUserSign", 'X-Requested-With=XMLHttpRequest')  
        
        for i in range(1,4):
            res = accessUrl("http://www.ictr.cn/Lottery/PostLotterys", 'activityId=8874409a-b6a9-49dd-8955-b2830ddd7ced')  
            #if not parseLotteryResult(res):
            if res.find("\"Success\":false") > 0:
                print("No more lotteries!")
                break
            m, b = parseLotteryResult(res)
            money += m
            beans += b
            print("Lotery %d, result:%s"%(i, res))
            time.sleep(5)
        
        time.sleep(2)
        res = accessUrl("http://www.ictr.cn/RegisterUser/Logout")       
        time.sleep(7)
    
    print("Lotteries all complete, get money: [%d], beans: [%d]"%(money, beans))
    
if __name__=='__main__':
    main()
    