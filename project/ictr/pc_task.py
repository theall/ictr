#!/usr/bin/env Python
# coding=utf-8
import urllib.request as urllib2
import time
import hashlib
import json
import xml.sax
import re
import utils

def getLoginBody(user, password, versionCode):
    tpl = """<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><RegistUserLoginFRGroup xmlns="http://tempuri.org/"><Mobile>%s</Mobile><PassWord>%s</PassWord><UserId /><Groupid>9999</Groupid><MeterType>1</MeterType><Type>EmailAndPassWord</Type><validateCode /><CheckCode /><CollectionVersion>%s</CollectionVersion></RegistUserLoginFRGroup></soap:Body></soap:Envelope>"""
    return tpl%(user, hashlib.md5(password.encode()).hexdigest(), versionCode)

def getVersionBody():
    return """<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><get_PC_version xmlns="http://tempuri.org/" /></soap:Body></soap:Envelope>"""
    
def findMatch(source, pattern):
    p = re.compile(pattern)
    ret = re.findall(p, source)
    if type(ret)==list and len(ret)>0:
        return ret[-1]
    return ''
    
def getUseridToken(s):
    userid = findMatch(s, r"\\\"UserGuid\\\":\\\"(.+?)\\\"")
    token = findMatch(s, r"\\\"TokenCode\\\":\\\"(.+?)\\\"")        
    return (userid, token)
    
def getVersionCode(s):
    return findMatch(s, r"\<get_PC_versionResult\>(.+?)\</get_PC_versionResult\>")
    
def fillHeader(actionUrl):
    return {
        "Expect": "100-continue",
        "Connection": "Keep-Alive",
        "User-Agent": "Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 2.0.50727.5485)",
        "Accept-Encoding": "gzip, deflate",
        "SOAPAction": actionUrl,
        "Content-Type": "text/xml; charset=utf-8"
    }
        
def checkResult(res):
    return res.find("\"Success\":true")>0
    
def postAction(remoteUrl, soapAction, body=''):
    res = ''
    try:
        req = urllib2.Request(url=remoteUrl, headers=fillHeader(soapAction), data=body.encode())
        ret_data = urllib2.urlopen(req)
        res = ret_data.read().decode()
    except Exception as e:
        print(e)
    return res
          
def main():
    accounts_list = utils.findAccountsList()
    if len(accounts_list)<1:
        print("No accounts found!")
        return
    else:
        print("Find %d accounts"%len(accounts_list))
    
    res = postAction("http://cn2service.ictr.cn/CN2AppForPCMeterService.asmx", 'http://tempuri.org/get_PC_version', getVersionBody())

    versionCode = getVersionCode(res)
    if versionCode=='':
        print("Version is empty!")
        return
    else:
        print("Pc version: %s\n"%versionCode)
        
    for account in accounts_list:
        res = postAction("http://cn2service.ictr.cn/CN2AppForPCMeterService.asmx", 'http://tempuri.org/RegistUserLoginFRGroup', getLoginBody(account[0], account[1], versionCode))

        if not checkResult(res):
            print("Login failed!Number: %s\n%s"%(account[0], res))
            continue
        else:
            print("Login success!Number: %s"%account[0])
        
        userid, token = getUseridToken(res)
        if userid=='' or token=='':
            print("Userid or token is empty, return body\n%s"%res)
            continue
        
        time.sleep(5)
        
if __name__=='__main__':
    main()