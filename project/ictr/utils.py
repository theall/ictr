﻿#!/usr/bin/env Python
# coding=utf-8
import random  
import re
import os
import logging

g_accounts_list = []

def setLoggingFile(filename):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)  # Log等级总开关
    
    # 创建一个handler，用于写入日志文件
    rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
    log_path = os.path.dirname(os.getcwd()) + '/Logs/'
    log_name = log_path + rq + '_' + filename + '.log'
    logfile = log_name
    fh = logging.FileHandler(filename, mode='w')
    fh.setLevel(logging.DEBUG)  # 输出到file的log等级的开关

    # 定义handler的输出格式
    formatter = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s")
    fh.setFormatter(formatter)
    logger.addHandler(fh)
        
def saveResult(res):
    f=open('z:/a.txt','w')
    f.write(res)
    f.close()
    
def findMatchAsString(s, p):
    pattern = re.compile(p)
    ret = re.findall(pattern, s)
    if type(ret)==list and len(ret)>0:
        return ret[-1]
    return ''

def findMatchAsInteger(s, p):
    ret = findMatchAsString(s, p)
    if ret!='':
        ret = int(ret)
    else:
        ret = 0
    return ret
    
#先从本机查密码，没有的话再从远程获取
def findAccountsList(dataFile = 'pass.bin'):
    global g_accounts_list
    if len(g_accounts_list) > 0:
        random.shuffle(g_accounts_list)
        return g_accounts_list
        
    ret = []
    accounts_list = []
    if os.path.exists(dataFile):
        accountsFile = open(dataFile)
        accounts_list = accountsFile.readlines()
    else:
        import urllib.request as urllib2
        res = ''
        try:
            header = {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'}
            req = urllib2.Request(url="https://www.stringex.cn/pass.bin", headers=header) 
            ret_data = urllib2.urlopen(req)
            res = ret_data.read().decode('gb2312')
            #print(res)
            accounts_list = res.split('\n')
        except Exception as e:
            print(e)
        
    if type(accounts_list)==list and len(accounts_list)>0:
        for account in accounts_list:
            if account=='' or account[0] in [' ', '\t', '#']:
                continue
            name, password = account.split(' ')
            name = name.strip()
            password = password.strip()
            ret.append([name, password])
    if len(ret) > 0:
        random.shuffle(ret)
    g_accounts_list = ret
    return ret
    
if __name__=='__main__':
    print(findAccountsList())