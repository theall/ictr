#!/usr/bin/env Python
# coding=utf-8
import urllib.request as urllib2
import time
import hashlib
import json
import xml.sax
import re
import utils

def getLoginBody(user, password):
    tpl = """\
    <v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><RegistUserLoginFRGroup xmlns="http://cn2service.ictr.cn/" id="o0" c:root="1"><Mobile i:type="d:string">%s</Mobile><PassWord i:type="d:string">%s</PassWord><Groupid i:type="d:string">600</Groupid><MeterType i:type="d:string">2</MeterType></RegistUserLoginFRGroup></v:Body></v:Envelope>"""
    return tpl%(user, hashlib.md5(password.encode()).hexdigest())

def getLogoutBody():
    return """<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><GetVersionInfoNew xmlns="http://st.ictr.cn:80/" id="o0" c:root="1"><name i:type="d:string">VersionCode</name></GetVersionInfoNew></v:Body></v:Envelope>"""
    
def getSignBody(userid, version, token):
    return """<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><RegistUserSignForGroup xmlns="http://cn2service.ictr.cn/" id="o0" c:root="1"><UserId i:type="d:string">%s</UserId><SignType i:type="d:string">2</SignType><version i:type="d:string">%s</version><TokenCode i:type="d:string">%s</TokenCode><GroupId i:type="d:string">600</GroupId></RegistUserSignForGroup></v:Body></v:Envelope>"""%(userid, hashlib.md5(version.encode()).hexdigest(), token)
    
def findMatch(source, pattern):
    p = re.compile(pattern)
    ret = re.findall(p, source)
    if type(ret)==list and len(ret)>0:
        return ret[-1]
    return ''
    
def getUseridToken(s):
    userid = findMatch(s, r"\\\"UserGuid\\\":\\\"(.+?)\\\"")
    token = findMatch(s, r"\\\"TokenCode\\\":\\\"(.+?)\\\"")        
    return (userid, token)
    
def getVersionCode(s):
    return findMatch(s, r"\<GetVersionInfoNewResult\>(.+?)\</GetVersionInfoNewResult\>")
    
def fillHeader(actionUrl):
    return {
        'User-Agent': 'ksoap2-android/2.6.0+',
        'SOAPAction': actionUrl,
        'Accept-Encoding': 'gzip',
        'Content-Type': 'text/xml;charset=utf-8'
    }
       
def checkResult(res):
    return res.find("\"Success\":true")>0
    
    try:
        res = json.loads(res)
        if type(res)!=dict and not res.get('Success', False):
            return False
        return True
    except:
        pass
    return False
    
def postAction(remoteUrl, soapAction, body=''):
    res = ''
    try:
        req = urllib2.Request(url=remoteUrl, headers=fillHeader(soapAction), data=body.encode())
        ret_data = urllib2.urlopen(req)
        res = ret_data.read().decode()
    except Exception as e:
        print(e)
    return res
           
def main():
    accounts_list = utils.findAccountsList()
    if len(accounts_list)<1:
        print("No accounts found!")
        return
    else:
        print("Find %d accounts"%len(accounts_list))
    
    versionCode = ''
    for account in accounts_list:
        res = postAction("http://cn2service.ictr.cn/CN2AppMobileMeterWebService.asmx", 'http://cn2service.ictr.cn/RegistUserLoginFRGroup', getLoginBody(account[0], account[1]))

        if not checkResult(res):
            print("Login failed!Number: %s\n%s"%(account[0], res))
            continue
        else:
            print("Login success!Number: %s"%account[0])
        
        userid, token = getUseridToken(res)
        if userid=='' or token=='':
            print("Userid or token is empty, return body\n%s"%res)
            continue
        
        # get version code
        if versionCode=='':
            res = postAction("http://st.ictr.cn/STWS.asmx", "http://st.ictr.cn:80/GetVersionInfoNew", getLogoutBody())
            versionCode = getVersionCode(res)
            if versionCode=='':
                print("No version code!\n%s"%res)
                break
            time.sleep(2)
        
        # Sign
        res = postAction("http://cn2service.ictr.cn/CN2AppMobileMeterWebService.asmx", 'http://cn2service.ictr.cn/RegistUserSignForGroup', getSignBody(userid, versionCode, token))  
        if not checkResult(res):
            pass
            print("Signed!")
        else:
            print("Sign success!")
            
        time.sleep(2)
        res = postAction("http://st.ictr.cn/STWS.asmx", "http://st.ictr.cn:80/GetVersionInfoNew", getLogoutBody())
        print("Logout!")
        
        time.sleep(5)
        
        
if __name__=='__main__':
    main()