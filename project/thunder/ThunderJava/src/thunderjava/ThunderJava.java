/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thunderjava;

/**
 *
 * @author Administrator
 */
public class ThunderJava {

    native void hello();  
      
    static {  
        System.loadLibrary("net");  
    }  
    
    public native String stringFromJNI(long context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ThunderJava helloJni = new ThunderJava();  
        helloJni.stringFromJNI(0, 
                "", 
                "1528615989", 
                "43b6fdc7-bb91-37fa-98bb-39913019d79f", 
                "869026021276576", 
                "AE0AAC76FE73", "5251ab169749a2fb", 
                "t-jinritoutiao-3", 
                "4.6.0", 
                "506a28c1-7764-41ec-9a00-35e01ab297fc");
    }
    
}
