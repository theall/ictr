#!/usr/bin/env Python
# coding=utf-8
import urllib.request as urllib2
import time
import json
import re
import utils
import base64
import uuid

"""Cookie: device_id=43b6fdc7-bb91-37fa-98bb-39913019d79f;device_id=43b6fdc7-bb91-37fa-98bb-39913019d79f;mac_addr=;android_id=5251ab169749a2fb;imei=869026021276576;device_serial=23526082202791;wifi_mac_addr=AE0AAC76FE73
"""
"""http://api.liquidnetwork.com/user/login/password_submit?version_name=4.6.0&device_id=43b6fdc7-bb91-37fa-98bb-39913019d79f&channel_name=t-jinritoutiao-3&et=1528615989&device_serial=23526082202791&nonce_str=506a28c1-7764-41ec-9a00-35e01ab297fc&sign=2R8qAhrcLO-db0QaZ91LLQ&user_id=&yid=&Latitude=&Longitude=&black_box=eyJvcyI6ImFuZHJvaWQiLCJ2ZXJzaW9uIjoiMy4wLjkiLCJwYWNrYWdlcyI6ImMubC5hXzQuNi4wIiwicHJvZmlsZV90aW1lIjozMjk2NCwiaW50ZXJ2YWxfdGltZSI6MTk0MDUxLCJ0b2tlbl9pZCI6ImltTnBGWE5pWHdRNGxNOWRwY2RiZklOZ1BlQkJ0a1NJWWp1NHVDVFBScGUwMGhGWSt2V2V3UFRIaWRLNkkxdHRyU1YxNHFBWERlR2FhS0lWQmpybEZBPT0ifQ%3D%3D"""

def getDeviceId():
    return "43b6fdc7-bb91-37fa-98bb-39913019d79f"
    
def getAndroidId():
    return "5251ab169749a2fb"
    
def ٴgetIMEI():
    return "869026021276576"
    
def getDeviceSerial():
    return "23526082202791"
    
def getWifiMacAddr():
    return "AE0AAC76FE73"
    
def getChannelName():
    return "t-jinritoutiao-3"
    
def getClientVersion():
    return "4.6.0"
    
def strToByte(s):
    bArr = [0 for i in range(int(len(s) / 2))]
    toCharArray = s
    iArr = [0, 0]
    i = 0
    i2 = 0
    while (i2 < len(bArr) * 2):
        i3 = 0
        while (i3 < 2):
            _s = toCharArray[i2 + i3]
            _ord_s = ord(_s)
            if (_s >= '0' and _s <= '9'):
                iArr[i3] = _ord_s - 48
            elif (_s >= 'A' and _s <= 'F'):
                iArr[i3] = (_ord_s - 65) + 10
            elif (_s >= 'a' and _s <= 'f'):
                iArr[i3] = (_ord_s - 97) + 10
            i3 += 1
        iArr[0] = (iArr[0] & 15) << 4
        iArr[1] = iArr[1] & 15
        bArr[i] = (iArr[0] | iArr[1]) & 0xff
        i2 += 2
        i += 1
    return bytes(bArr)
    
def getSignStr(str1, str2, str3):
    stringFromJNI = ""
    if str1 is None:
        str1 = ""
    #try:
    strArr = [str1, str2, getDeviceId(), ٴgetIMEI(), getWifiMacAddr(), getAndroidId(), getChannelName(), getClientVersion()]
    for i in range(len(strArr)):
        if (strArr[i] is None):
            strArr[i] = "null"
    strArr.append(str3)
    print(strArr)
    stringFromJNI = "".join(strArr)
    while (len(stringFromJNI) < 32):
        stringFromJNI = "0" + stringFromJNI
    stringFromJNI = base64.b64encode(strToByte(stringFromJNI))
    stringFromJNI = stringFromJNI.replace(b"+", b"-")
    stringFromJNI = stringFromJNI.replace(b"/", b"_")
    stringFromJNI = stringFromJNI.replace(b"=", b"")
    #except Exception as e:
    #    print(e)
    print(stringFromJNI)
    return stringFromJNI

def getLoginBody(user, password):
    tpl = "phone_number=%s&password=%s"
    return tpl%(user, password)

def getLogoutBody():
    return ""
    
    
def findMatch(source, pattern):
    p = re.compile(pattern)
    ret = re.findall(p, source)
    if type(ret)==list and len(ret)>0:
        return ret[-1]
    return ''
    
def getUseridYid(res):
    # get user id 
    #   :
    #      "code": 1, 
    #      "data"::
    #        "user_id": "68618922151", 
    #        "user_name": "13507214647", 
    #        "yid": "68618922151_0696427299"
    #      }, 
    #      "message": "\u767b\u5f55\u6210\u529f"
    #    userid = ''
    yid = ''
    try:
        res = json.loads(res)
        if type(res)==dict and res.get('message', '')=='登录成功':
            data = res['data']
            userid = data['user_id']
            yid = data['yid']
    except:
        pass 
    return (userid, yid)
    
def getVersionCode(s):
    return findMatch(s, r"\<GetVersionInfoNewResult\>(.+?)\</GetVersionInfoNewResult\>")

def fillHeader(req):
    req.add_header('Host', 'api.liquidnetwork.com')
    req.add_header('Connection', 'keep-alive')
    req.add_header('User-Agent', 'Mozilla/5.0 (Linux; Android 5.1; M6 Build/LMY47D) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/39.0.0.0 Mobile Safari/537.36')
    req.add_header('Accept-Encoding', 'gzip')
    req.add_header('Content-Encoding', 'gzip')
    req.add_header('Accept-Charset', 'UTF-8')
    req.add_header('Accept', '*/*')
    req.add_header('Content-Type', 'application/x-www-form-urlencoded')
    return req
       
       
def checkResult(res):

    try:
        res = json.loads(res)
        if type(res)!=dict and not res.get('Success', False):
            return False
        return True
    except:
        pass
    return False
    
def postAction(remoteUrl, body=''):
    res = ''
    try:
        req = fillHeader(urllib2.Request(url=remoteUrl,data=body.encode()))
        ret_data = urllib2.urlopen(req)
        res = ret_data.read().decode('gb2312')
    except Exception as e:
        print(e)
    return res
           
def main():
    accounts_list = utils.findAccountsList()
    if len(accounts_list)<1:
        print("No accounts found!")
        return
    else:
        print("Find %d accounts"%len(accounts_list))
    
    for account in accounts_list:
        et = int(time.time() + 0.3)
        nonce_str = str(uuid.uuid1())
        signStr = getSignStr(None, str(et), nonce_str)
        res = postAction("http://api.liquidnetwork.com/user/login/password_submit?version_name=4.6.0&device_id=43b6fdc7-bb91-37fa-98bb-39913019d79f&channel_name=t-jinritoutiao-3&et=%d&device_serial=23526082202791&nonce_str=%s&sign=%s&user_id=&yid=&Latitude=&Longitude=&black_box=eyJvcyI6ImFuZHJvaWQiLCJ2ZXJzaW9uIjoiMy4wLjkiLCJwYWNrYWdlcyI6ImMubC5hXzQuNi4wIiwicHJvZmlsZV90aW1lIjo3NjYsImludGVydmFsX3RpbWUiOjIyMzk4NCwidG9rZW5faWQiOiJyWGZKdU00QzhlcXRMbkJRQnRzT3BuVHNvUjBrVHVsbUFZdjRkVTBEUDFmVXpYRFRBb2xOU3NvZjRwdDl2cE1pN3RVSldXSjZabmV6NlRSNWVrMEhBZz09In0%3D"%(et, nonce_str, signStr), getLoginBody(account[0], account[1]))

        userId, Yid = getUseridYid(res)
        if Yid=='' or userId=='':
            print("Login failed!Number: %s\n%s"%(account[0], res))
            continue
        else:
            print("Login success!Number: %s"%account[0])
        
        continue
        # get version code
        if versionCode=='':
            res = postAction("http://st.ictr.cn/STWS.asmx", "http://st.ictr.cn:80/GetVersionInfoNew", getLogoutBody())
            versionCode = getVersionCode(res)
            if versionCode=='':
                print("No version code!\n%s"%res)
                break
            time.sleep(2)
        
        # Sign
        res = postAction("http://cn2service.ictr.cn/CN2AppMobileMeterWebService.asmx", 'http://cn2service.ictr.cn/RegistUserSignForGroup', getSignBody(userid, versionCode, token))  
        if not checkResult(res):
            pass
            print("Signed!")
        else:
            print("Sign success!")
            
        time.sleep(2)
        res = postAction("http://st.ictr.cn/STWS.asmx", "http://st.ictr.cn:80/GetVersionInfoNew", getLogoutBody())
        print("Logout!")
        
        time.sleep(5)
        
"""http://api.liquidnetwork.com/user/login/password_submit?version_name=4.6.0&device_id=43b6fdc7-bb91-37fa-98bb-39913019d79f&channel_name=t-jinritoutiao-3&et=1528615989&device_serial=23526082202791&nonce_str=506a28c1-7764-41ec-9a00-35e01ab297fc&sign=2R8qAhrcLO-db0QaZ91LLQ&user_id=&yid=&Latitude=&Longitude=&black_box=eyJvcyI6ImFuZHJvaWQiLCJ2ZXJzaW9uIjoiMy4wLjkiLCJwYWNrYWdlcyI6ImMubC5hXzQuNi4wIiwicHJvZmlsZV90aW1lIjozMjk2NCwiaW50ZXJ2YWxfdGltZSI6MTk0MDUxLCJ0b2tlbl9pZCI6ImltTnBGWE5pWHdRNGxNOWRwY2RiZklOZ1BlQkJ0a1NJWWp1NHVDVFBScGUwMGhGWSt2V2V3UFRIaWRLNkkxdHRyU1YxNHFBWERlR2FhS0lWQmpybEZBPT0ifQ%3D%3D"""
if __name__=='__main__':
    #getSignStr('http://api.liquidnetwork.com/user/login/password_submit', '1528615989', '506a28c1-7764-41ec-9a00-35e01ab297fc') #2R8qAhrcLO-db0QaZ91LLQ
    # sign=2R8qAhrcLO-db0QaZ91LLQ
    main()